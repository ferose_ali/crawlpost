import datetime
import tweepy
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class TwitterUser(models.Model):
    """
    This modal used to keep the public twitter handle
    """
    username = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = 'Public Twitter Handle'
        verbose_name_plural = 'Public Twitter Handles'

    def __str__(self):
        return self.username


class TwitterDetail(models.Model):
    """
    This modal used to keep the basic details of a tweet of a public twitter hanlde
    """
    username = models.CharField(max_length=255, blank=True, null=True)
    timeline_text = models.TextField(max_length=255, blank=True, null=True)
    hashtags = models.CharField(max_length=255, blank=True, null=True)
    created = models.CharField(null=True, blank=True, max_length=200)
    image = models.URLField(null=True, blank=True)

    class Meta:
        verbose_name = 'Tweet'
        verbose_name_plural = 'Tweets'
        ordering = ('-id',)

    def __str__(self):
        return (
            self.username if self.username else "Twitter data"
        ) + "  " + self.created


@receiver(post_save, sender=TwitterUser)
def fetch_posts(sender, instance, created, **kwargs):
    today = datetime.date.today()
    if created:
        auth = tweepy.auth.OAuthHandler(settings.CONSUMER_KEY, settings.CONSUMER_SECRET)
        auth.set_access_token(settings.ACCESS_TOKEN, settings.ACCESS_SECRET)
        try:
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
            new_tweets = api.user_timeline(screen_name=instance, count=10)
            for tweets in new_tweets:
                created = datetime.datetime.strptime(str(tweets.created_at), '%Y-%m-%d %H:%M:%S').date()
                if(today == created):
                    twitter_data = TwitterDetail.objects.filter(created=tweets.created_at)
                    if (twitter_data.exists()):
                        pass
                    else:
                        twitter_details = TwitterDetail(
                            username=instance,
                            timeline_text=tweets.text,
                            hashtags=tweets.entities.get('hashtags'),
                            created=tweets.created_at
                        )
                        if('media' in tweets.entities):
                            for img in tweets.entities.get('media'):
                                twitter_details.image = img['media_url']
                        twitter_details.save()
        except Exception as ex:
            print('Error "%s"' % ex)
