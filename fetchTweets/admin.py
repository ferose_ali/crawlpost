from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.utils.html import format_html
from .models import TwitterDetail, TwitterUser


class TwitterUsersAdmin(admin.ModelAdmin):
    search_fields = ['username']


class TwitterDetailsAdmin(admin.ModelAdmin):
    def image_tag(self, obj):
        if obj.image:
            return format_html('<img src="{}" height="150" width="260"/>'.format(obj.image))
        else:
            return None

    image_tag.short_description = 'Image'
    search_fields = ['username']
    list_display = ['username', 'timeline_text', 'hashtags', 'created', 'image_tag']
    list_filter = (
        ('created', admin.DateFieldListFilter),
        'username'
    )
    save_on_top = True


admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(TwitterUser, TwitterUsersAdmin)
admin.site.register(TwitterDetail, TwitterDetailsAdmin)
