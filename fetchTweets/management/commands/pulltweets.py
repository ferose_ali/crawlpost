import datetime
import tweepy
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from fetchTweets.models import TwitterUser, TwitterDetail

today = datetime.date.today()


class Command(BaseCommand):
    help = 'Tweets'

    def handle(self, *args, **options):
        auth = tweepy.auth.OAuthHandler(settings.CONSUMER_KEY, settings.CONSUMER_SECRET)
        auth.set_access_token(settings.ACCESS_TOKEN, settings.ACCESS_SECRET)
        try:
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
            twitter_handles = TwitterUser.objects.all().values_list('username', flat=True)
            for users in twitter_handles:
                new_tweets = api.user_timeline(screen_name=users, count=10)
                for tweets in new_tweets:
                    created = datetime.datetime.strptime(str(tweets.created_at), '%Y-%m-%d %H:%M:%S').date()
                    if(today == created):
                        twitter_data = TwitterDetail.objects.filter(created=tweets.created_at)
                        if (twitter_data.exists()):
                            pass
                        else:
                            twitter_details = TwitterDetail(
                                username=users,
                                timeline_text=tweets.text,
                                hashtags=tweets.entities.get('hashtags'),
                                created=tweets.created_at
                            )
                            if('media' in tweets.entities):
                                for img in tweets.entities.get('media'):
                                    twitter_details.image = img['media_url']
                            twitter_details.save()
        except Exception as ex:
            raise CommandError('Error "%s"' % ex)
