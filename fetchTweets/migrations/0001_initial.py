# Generated by Django 3.0 on 2020-12-02 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TwitterDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(blank=True, max_length=255, null=True)),
                ('timeline_text', models.TextField(blank=True, max_length=255, null=True)),
                ('hashtags', models.CharField(blank=True, max_length=255, null=True)),
                ('created', models.CharField(blank=True, max_length=200, null=True)),
                ('image', models.URLField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Tweet',
                'verbose_name_plural': 'Tweets',
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='TwitterUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'verbose_name': 'Public Twitter Handle',
                'verbose_name_plural': 'Public Twitter Handles',
            },
        ),
    ]
