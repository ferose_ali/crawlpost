# CrawlPost

CrawlPost is a tool to fetch the latest tweets from Twitter.

## How to run?

Create Virtualenv for Python3
```bash
python3 -m venv <env_name>
```
Activate the virtualenv

```bash
cd <env_name>
source bin/activate
```
Clone this project and setup the database in your system, then;

```bash
cd crawlpost
pip install -r requirements.txt
python manage.py runserver
```

Create super user using the following command and enter the details it asks.
```bash
python manage.py createsuperuser
```

Run the project
```bash
python manage.py runserver
```
Open the admin panel and enter the Twitter handles under "Public Twitter Handles" section. Then open another terminal and run the following command for fetching the tweets of the Twitter handles existing in the database.

```bash
python3 manage.py pulltweets
```

Set a cron job by creating a bash file and schedule the task for daily fetcing. 

Enjoy !!!